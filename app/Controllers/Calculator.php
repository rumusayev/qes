<?php namespace App\Controllers;

class Calculator extends BaseController
{
	public function run()
	{
        $validation =  \Config\Services::validation();

        $validation->setRules([
            'token' => 'required',
            'a' => 'required|is_natural_no_zero',
            'b' => 'required|integer',
            'c' => 'required|integer'
        ]);

        $validation->withRequest($this->request)
            ->run();

        if ($validation->getErrors())
        {
            return $this->response->setJSON([
                'error' => $validation->getErrors()
            ]);
        }

        return $this->validateToken($this->request);

	}

	public function validateToken($data)
    {
        if ($this->request->getVar('token') !== sha1($this->request->getVar('a') .
                $this->request->getVar('b') . $this->request->getVar('c'))) {
            return $this->response->setJSON([
                'error' => 'Token validation error'
            ]);
        }

        $db      = \Config\Database::connect();

        $builder = $db->table('tokens');

        $query = $builder->getWhere(
            [
                'token' => $this->request->getVar('token'),
                'a'  => $this->request->getVar('a'),
                'b'  => $this->request->getVar('b'),
                'c'  => $this->request->getVar('c')
            ]);

        if (is_null($query->getFirstRow())){
            $data = [
                'token' => $this->request->getVar('token'),
                'a'  => $this->request->getVar('a'),
                'b'  => $this->request->getVar('b'),
                'c'  => $this->request->getVar('c')
            ];

            $builder->insert($data);
        } else {
            $builder->update([
                'usage' => $query->getFirstRow()->usage + 1
            ]);
        }

        return $this->calculate();
    }

    private function calculate() {

        $d = $this->request->getVar('b') * $this->request->getVar('b') -
            4 * $this->request->getVar('a') * $this->request->getVar('c');

        if($d < 0) {
            return $this->response->setJSON([
                'error' => 'The equation has no real solutions!'
            ]);
        } elseif($d == 0) {
            return $this->response->setJSON([
                'solutions' => [
                    (-$this->request->getVar('b') / 2*$this->request->getVar('a'))
                ]
            ]);
        } else  {

            return $this->response->setJSON([
                'solutions' => [
                    ((-$this->request->getVar('b') + sqrt($d)) / (2*$this->request->getVar('a'))),
                    ((-$this->request->getVar('b') - sqrt($d)) / (2*$this->request->getVar('a')))
                ]
            ]);
        }

        \CodeIgniter\Events\Events::trigger('store_response',
            $this->request->getVar('token'),
            $this->request->getVar('a'),
            $this->request->getVar('b'),
            $this->request->getVar('c')
        );
    }

}
