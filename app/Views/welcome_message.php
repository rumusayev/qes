<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Welcome to CodeIgniter 4!</title>
	<meta name="description" content="The small framework with powerful features">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="/favicon.ico"/>

    <style>
        div {
            width: 300px;
            margin: 0 auto;
            padding: 20px 0;
        }
        input {
            width: 40px;
        }

        .centered {
            text-align: center;
        }

        #error {
            font-size: 24px;
            color: red;
        }

        #error > label {
            display: block;
        }

        #token {
            width: 100%;
        }

        .notification {
            display: none;
        }

        .blockError {
            color: red;
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" type="text/javascript"></script>
</head>
<body>

<div class="notification blockError">
    Error: <div></div>
</div>

<div class="notification blockSingleSolution">
    Single solution: <div></div>
</div>

<div class="notification blockTwoSolution">
    Two solutions: <div></div>
</div>

<div class="notification blockNoSolution">
    No solution: <div></div>
</div>

<div>
    <form id="checkFormula" method="post" action="#">
        <div class="centered">
            <div id="error"></div>
        </div>
        <div class="centered">
            <input id="valA" name="a" placeholder="a"> x<sup>2</sup> + <input id="valB" name="b" placeholder="b"> x + <input id="valC" name="c" placeholder="c"> = 0
        </div>
        <div class="centered">
            Auto token<input id="token" name="token" readonly>
        </div>
        <div class="centered">
            <button type="submit">Check</button>
        </div>
    </form>
</div>

<script>
    $(function() {

        $('#valA, #valB, #valC').keyup(function () {
            $('#token').val( sha1( $('#valA').val() + $('#valB').val() + $('#valC').val() ) )
        })

        var validator = $("#checkFormula").validate({
            rules: {
                a: {
                    required:true,
                    number: true,
                    notEqual: '0'
                },
                b: {
                    required:true,
                    number: true
                },
                c: {
                    required:true,
                    number: true
                }
            },
            messages: {
                a: {
                    required: "Please enter 'a' value",
                    number:"Please enter numbers only",
                    notEqual:"'a' can not be zero"
                },
                b: {
                    required: "Please enter 'b' value",
                    number:"Please enter numbers only"
                },
                c: {
                    required: "Please enter 'c' value",
                    number:"Please enter numbers only"
                }
            },
            submitHandler: function() {
            },
            errorPlacement: function(error, element) {
                error.appendTo('#error');
            }
        });

        $("#checkFormula").submit(function(){
            var token = $('#token').val();
            if(validator.valid()){
                $.ajax({
                    url: "/calculate",
                    method: 'POST',
                    data: $('#checkFormula').serializeArray(),
                    success: function(result){
                        $('.notification').hide();
                        if (result.error) {
                            $('.blockNoSolution > div').text(result.error)
                            $('.blockNoSolution').show();
                        } else if (result.solutions && Object.keys(result.solutions).length == 1) {
                            console.log(1);
                            var ul = $("<ul>");
                            ul.append("<li>x: " + result.solutions[0] + "</li>");
                            $('.blockSingleSolution > div').html(ul);
                            $('.blockSingleSolution').show();
                        } else if (result.solutions && Object.keys(result.solutions).length == 2) {
                            var ul = $("<ul>");
                            ul.append("<li>x1: " + result.solutions[0] + "</li>");
                            ul.append("<li>x2: " + result.solutions[1] + "</li>");
                            $('.blockTwoSolution > div').html(ul)
                            $('.blockTwoSolution').show();
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        console.log(XMLHttpRequest);
                        $('.blockError > div').text(XMLHttpRequest.status + " " + XMLHttpRequest.statusText)
                        $('.blockError').show();
                    }});
            }
            return false;
        });

        $.validator.addMethod("notEqual", function (value, element, param) { // Adding rules for Amount(Not equal to zero)
            return this.optional(element) || value != '0';
        });

        // UTF-8 encode / decode by Johan Sundstr?m
        function encode_utf8( s )
        {
            return unescape( encodeURIComponent( s ) );
        }

        function decode_utf8( s )
        {
            return decodeURIComponent( escape( s ) );
        }

        function sha1(str) {

            var rotate_left = function(n, s) {
                var t4 = (n << s) | (n >>> (32 - s));
                return t4;
            };

            var cvt_hex = function(val) {
                var str = '';
                var i;
                var v;

                for (i = 7; i >= 0; i--) {
                    v = (val >>> (i * 4)) & 0x0f;
                    str += v.toString(16);
                }
                return str;
            };

            var blockstart;
            var i, j;
            var W = new Array(80);
            var H0 = 0x67452301;
            var H1 = 0xEFCDAB89;
            var H2 = 0x98BADCFE;
            var H3 = 0x10325476;
            var H4 = 0xC3D2E1F0;
            var A, B, C, D, E;
            var temp;

            str = encode_utf8(str);
            var str_len = str.length;

            var word_array = [];
            for (i = 0; i < str_len - 3; i += 4) {
                j = str.charCodeAt(i) << 24 | str.charCodeAt(i + 1) << 16 | str.charCodeAt(i + 2) << 8 | str.charCodeAt(i + 3);
                word_array.push(j);
            }

            switch (str_len % 4) {
                case 0:
                    i = 0x080000000;
                    break;
                case 1:
                    i = str.charCodeAt(str_len - 1) << 24 | 0x0800000;
                    break;
                case 2:
                    i = str.charCodeAt(str_len - 2) << 24 | str.charCodeAt(str_len - 1) << 16 | 0x08000;
                    break;
                case 3:
                    i = str.charCodeAt(str_len - 3) << 24 | str.charCodeAt(str_len - 2) << 16 | str.charCodeAt(str_len - 1) <<
                        8 | 0x80;
                    break;
            }

            word_array.push(i);

            while ((word_array.length % 16) != 14) {
                word_array.push(0);
            }

            word_array.push(str_len >>> 29);
            word_array.push((str_len << 3) & 0x0ffffffff);

            for (blockstart = 0; blockstart < word_array.length; blockstart += 16) {
                for (i = 0; i < 16; i++) {
                    W[i] = word_array[blockstart + i];
                }
                for (i = 16; i <= 79; i++) {
                    W[i] = rotate_left(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);
                }

                A = H0;
                B = H1;
                C = H2;
                D = H3;
                E = H4;

                for (i = 0; i <= 19; i++) {
                    temp = (rotate_left(A, 5) + ((B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
                    E = D;
                    D = C;
                    C = rotate_left(B, 30);
                    B = A;
                    A = temp;
                }

                for (i = 20; i <= 39; i++) {
                    temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
                    E = D;
                    D = C;
                    C = rotate_left(B, 30);
                    B = A;
                    A = temp;
                }

                for (i = 40; i <= 59; i++) {
                    temp = (rotate_left(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
                    E = D;
                    D = C;
                    C = rotate_left(B, 30);
                    B = A;
                    A = temp;
                }

                for (i = 60; i <= 79; i++) {
                    temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
                    E = D;
                    D = C;
                    C = rotate_left(B, 30);
                    B = A;
                    A = temp;
                }

                H0 = (H0 + A) & 0x0ffffffff;
                H1 = (H1 + B) & 0x0ffffffff;
                H2 = (H2 + C) & 0x0ffffffff;
                H3 = (H3 + D) & 0x0ffffffff;
                H4 = (H4 + E) & 0x0ffffffff;
            }

            temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
            return temp.toLowerCase();
        }
    });
</script>

</body>
</html>
